import { Component, HostListener } from '@angular/core';
import { GridStack } from 'gridstack';
import 'gridstack/dist/gridstack.min.css';
import 'gridstack/dist/h5/gridstack-dd-native';
declare var require: any;
import * as Highcharts from 'highcharts';
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-dashboard';

  options = { // put in gridstack options here
    disableOneColumnMode: false,
    float: true,
    removable: false,
    disableDrag: false,
    disableResize: false,
    resizable: { autoHide: true, handles: 'all' },
  };

  addWidget() {
    var grid = GridStack.init(this.options);
    var id = "ContainerWidget" + grid.getGridItems().length;
    var childId = grid.getGridItems().length;
    grid.addWidget('<div class="grid-stack-item" id="' + childId + '"><div class="grid-stack-item-content"><div class="p-2"><div class="container"  id = "' + id + '"><button id="graph' + id + '">Add Graph</button></div></div></div></div>', { w: 3 })

    grid.on('resizestop', event => {
      if (Highcharts.charts.find(v => v['renderTo'].id == "ContainerWidget" + (<HTMLInputElement>event.target).id)) {
        Highcharts.charts.find(v => v['renderTo'].id == "ContainerWidget" + (<HTMLInputElement>event.target).id).reflow();
      }

    })

  }




  getChartOptions() {
    return {
      chart: {
        type: 'column',
        reflow: true
      },
      title: {
        text: ''
      },
      credits: {
        enabled: false
      },

      xAxis: {
        categories: [
          'Jan',
          'Feb',
          'Mar',
        ],
        crosshair: true
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Rainfall (mm)'
        }
      },
      tooltip: {
        enabled: true,
        useHtML: true,
        followPointer: true,
      },
      exporting:
      {
        enabled: false
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: [{
        type: undefined,
        name: 'Mumbai',
        data: [49, 71, 106.4]

      }]
    }
  }
  chart: Highcharts.Chart
  chartType = 'column'
  @HostListener('document:click', ['$event.target'])
  addGraph(target) {
    if (target?.id?.includes("graphContainerWidget")) {
      // this.clickedId = target.id.substring(5);
      var options = this.getChartOptions();
      switch (this.chartType) {
        case 'column':
          options.chart.type = 'bar'
          this.chartType = 'bar'
          break;
        case 'bar':
          options.chart.type = 'pie'
          this.chartType = 'pie'
          break;
        case 'pie':
          options.chart.type = 'line'
          this.chartType = 'line'
          break;
        case 'line':
          options.chart.type = 'column'
          this.chartType = 'column'
          break;
        default:
          break;
      }
      this.chart = Highcharts.chart(target.id.substring(5), options)
    }
  }

}
